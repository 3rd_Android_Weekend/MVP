package com.kshrd.mvp.callback;

/**
 * Created by pirang on 8/19/17.
 */

public interface Callback {

    void onSuccess();
    void onFailed();

}
