package com.kshrd.mvp.ui.main.mvp;

import com.kshrd.mvp.callback.Callback;

/**
 * Created by pirang on 8/19/17.
 */

public class MainActivityInteractor implements MainActivityMvp.Interactor {

    private String email = "admin@gmail.com";
    private String password= "12345678";

    @Override
    public void login(String email, String password, Callback callback) {
        if (email.equals(this.email) && password.equals(this.password)){
            callback.onSuccess();
        } else {
            callback.onFailed();
        }
    }


}
