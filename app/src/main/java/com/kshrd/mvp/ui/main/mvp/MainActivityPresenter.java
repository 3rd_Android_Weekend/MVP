package com.kshrd.mvp.ui.main.mvp;

import com.kshrd.mvp.base.BasePresenter;
import com.kshrd.mvp.callback.Callback;

/**
 * Created by pirang on 8/19/17.
 */

public class MainActivityPresenter extends BasePresenter<MainActivityMvp.View> implements MainActivityMvp.Presenter {

    private MainActivityMvp.Interactor interactor;

    public MainActivityPresenter() {
        interactor = new MainActivityInteractor();
    }

    @Override
    public void login(String email, String password) {
        interactor.login(email, password, new Callback() {
            @Override
            public void onSuccess() {
                getMvpView().onLoginSuccess();
            }

            @Override
            public void onFailed() {
                getMvpView().onLoginFailed();
            }
        });
    }




}
