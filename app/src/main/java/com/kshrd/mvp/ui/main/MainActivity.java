package com.kshrd.mvp.ui.main;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import com.kshrd.mvp.R;
import com.kshrd.mvp.base.BaseActivity;
import com.kshrd.mvp.ui.main.mvp.MainActivityMvp;
import com.kshrd.mvp.ui.main.mvp.MainActivityPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements MainActivityMvp.View {

    private MainActivityMvp.Presenter presenter;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @BindView(R.id.etPassword)
    EditText etPassword;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUnbinder(ButterKnife.bind(this));

        presenter = new MainActivityPresenter();
        presenter.onAttach(this);
        dialog = new ProgressDialog(this);
        dialog.setTitle("Loading...");
    }

    @Override
    public void showLoading() {
        dialog.show();
    }

    @Override
    public void hideLoading() {
        dialog.hide();
    }

    @OnClick(R.id.btnLogin)
    public void onLoginClicked() {
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        presenter.login(email, password);
    }

    @Override
    public void onLoginSuccess() {
        Toast.makeText(this, "Success", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLoginFailed() {
        Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDetach();
    }
}
