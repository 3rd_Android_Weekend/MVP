package com.kshrd.mvp.ui.main.mvp;

import com.kshrd.mvp.base.BaseMvpPresenter;
import com.kshrd.mvp.base.BaseMvpView;
import com.kshrd.mvp.callback.Callback;

/**
 * Created by pirang on 8/19/17.
 */

public interface MainActivityMvp {

    interface View extends BaseMvpView {
        void onLoginSuccess();

        void onLoginFailed();
    }

    interface Presenter extends BaseMvpPresenter<MainActivityMvp.View> {
        void login(String email, String password);
    }

    interface Interactor {
        void login(String email, String password, Callback callback);
    }
}
