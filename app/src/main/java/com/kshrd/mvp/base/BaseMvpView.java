package com.kshrd.mvp.base;

/**
 * Created by pirang on 8/20/17.
 */

public interface BaseMvpView {
    void showLoading();
    void hideLoading();
}
