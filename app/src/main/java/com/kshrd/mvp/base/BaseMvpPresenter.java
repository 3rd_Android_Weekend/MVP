package com.kshrd.mvp.base;

/**
 * Created by pirang on 8/20/17.
 */

public interface BaseMvpPresenter<V extends BaseMvpView> {

    void onAttach(V mvpView);
    void onDetach();

}
