package com.kshrd.mvp.base;

/**
 * Created by pirang on 8/20/17.
 */

public abstract class BasePresenter<V extends BaseMvpView> implements BaseMvpPresenter<V> {

    private V mvpView;

    @Override
    public void onAttach(V mvpView) {
        this.mvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mvpView = null;
    }

    public V getMvpView() {
        return mvpView;
    }
}
